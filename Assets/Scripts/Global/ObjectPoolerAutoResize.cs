﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolerAutoResize<T> where T : MonoBehaviour
{
    private List<T> _available = new List<T>();
    private List<T> _inUse = new List<T>();

    private static ObjectPoolerAutoResize<T> _instance = null;
    public static ObjectPoolerAutoResize<T> Instance
    {
        get {
            if (_instance == null)
                _instance = new ObjectPoolerAutoResize<T>();
            return _instance;
        }
    }

    public T TakeFromPool(T prefab)
    {
        T go = null;

        if (_available.Count != 0)
        {
            go = _available[0];
            _available.RemoveAt(0);
            _inUse.Add(go);
            return go;
        }
        else
        {
            go = GameObject.Instantiate(prefab);
            _inUse.Add(go);
            return go;
        }
    }

    public void ReleaseToPool(T prefab)
    {
        if (_inUse.Contains(prefab))
        {
            _inUse.Remove(prefab);
            _available.Add(prefab);
        }
        prefab.gameObject.SetActive(false);
    }
}