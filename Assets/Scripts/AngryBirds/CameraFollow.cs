﻿using UnityEngine;

namespace Assets.Scripts.AngryBirds
{
    public class CameraFollow : MonoBehaviour
    {
        private const int _smoothnes = 2;
        private Transform _currentObjectToFollow;

        private void LateUpdate()
        {
            if (_currentObjectToFollow == null) return;

            var followPosition = Vector3.Lerp(transform.position, _currentObjectToFollow.position, Time.deltaTime * _smoothnes);
            followPosition.y = transform.position.y;
            followPosition.z = transform.position.z;
            transform.position = Vector3.MoveTowards(transform.position, followPosition, 0.5f);

        }

        public void StartFollow(Transform objectToFollow)
        {
            _currentObjectToFollow = objectToFollow;
        }

        public void Reset()
        {
            _currentObjectToFollow = null;            
        }
    }
}
