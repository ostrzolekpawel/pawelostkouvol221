﻿using UnityEngine;

namespace Assets.Scripts.AngryBirds
{

    public class Slingshot : MonoBehaviour
    {
        [SerializeField] private CameraFollow _cameraFollow;
        [SerializeField] private Cord _cord;
        //[SerializeField] private Bird _bird;
        [SerializeField] private Bird _birdPrefab;
        [SerializeField] private Transform _cetner;
        [SerializeField] private float _maxStretch;

        private Camera _camera;
        private Vector3 _holdPosition = Vector3.zero;
        private Bird _currentBird = null;

        private void Awake()
        {
            _camera = Camera.main;
        }

        private void OnMouseDown()
        {
            _holdPosition = TakeMousePosition();
            SpawnBirdAtPosition(_holdPosition);
        }

        private void SpawnBirdAtPosition(Vector3 position)
        {
            //if (_currentBird != null) return;
            _currentBird = Instantiate<Bird>(_birdPrefab, position, Quaternion.identity);
        }

        private Vector3 TakeMousePosition()
        {
            var position = _camera.ScreenToWorldPoint(Input.mousePosition);
            position.z = 0;
            return position;
        }

        private void OnMouseDrag()
        {
            _holdPosition = TakeMousePosition();
            _holdPosition = _cetner.position + Vector3.ClampMagnitude((_holdPosition - _cetner.position), _maxStretch);
            ShootManger.Instance.NotifyStretch(_holdPosition);
        }
        private void OnMouseUp()
        {
            _cord.Reset();
            ShootManger.Instance.NotifyShoot(_holdPosition);
            Debug.Log($"mouse up: {_holdPosition}");
            _cameraFollow.StartFollow(_currentBird.transform);
        }
    }

}