﻿using System;
using UnityEngine;

namespace Assets.Scripts.AngryBirds
{
    public class ShootManger : MonoSingleton<ShootManger>
    {
        public event Action<Vector3> OnShoot;
        public event Action<Vector3> OnStretch;


        public void NotifyShoot(Vector3 vector)
        {
            OnShoot?.Invoke(vector);
        }

        public void NotifyStretch(Vector3 vector)
        {
            OnStretch?.Invoke(vector);
        }
    }
}
