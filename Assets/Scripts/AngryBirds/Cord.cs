﻿using Assets.Scripts.AngryBirds;
using UnityEngine;
namespace Assets.Scripts.AngryBirds
{

    public class Cord : MonoBehaviour
    {
        [SerializeField] private Transform _idleEndPosition;

        [SerializeField] private Rope _backRope;
        [SerializeField] private Rope _frontRope;

        private void Start()
        {
            _backRope.Init(_idleEndPosition.position);
            _frontRope.Init(_idleEndPosition.position);

            ShootManger.Instance.OnStretch += Stretch;
        }

        public void Stretch(Vector3 holdPosition)
        {
            _backRope.Stretch(holdPosition);
            _frontRope.Stretch(holdPosition);
        }

        public void Reset()
        {
            Stretch(_idleEndPosition.position);
        }
    } 
}