﻿using System;
using UnityEngine;
namespace Assets.Scripts.AngryBirds
{

    public class Rope : MonoBehaviour
    {
        [SerializeField] private LineRenderer _lineRenderer;
        [SerializeField] private Transform _startTransform;
        private Vector3 _startPosition;
        private Vector3 _idlePosition;

        private void Awake()
        {
            if (_lineRenderer == null)
                throw new NullReferenceException("There is no lineRenderer");
            _startPosition = _startTransform.position;
            _lineRenderer.SetPosition(0, _startPosition);
        }

        public void Init(Vector3 idlePosition)
        {
            _idlePosition = idlePosition;
            _lineRenderer.SetPosition(1, _idlePosition);
        }

        public void Stretch(Vector3 holdPosition)
        {
            _lineRenderer.SetPosition(1, holdPosition);
        }
    } 
}