﻿using UnityEngine;
namespace Assets.Scripts.AngryBirds
{

    [RequireComponent(typeof(Rigidbody2D))]
    public class Bird : MonoBehaviour
    {
        private Rigidbody2D _rigidbody;
        private ShootManger _shootManager;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _shootManager = ShootManger.Instance;
            _shootManager.OnShoot += Shoot;
            _shootManager.OnStretch += Stretch;;
        }

        public void Shoot(Vector3 force)
        {
            _rigidbody.velocity = -force.normalized * (force.magnitude * 2f);;
            _rigidbody.simulated = true;
        }

        public void Stretch(Vector3 position)
        {
            transform.position = position;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            _shootManager.OnShoot -= Shoot;
            _shootManager.OnStretch -= Stretch;
        }
    }

}