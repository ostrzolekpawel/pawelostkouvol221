﻿using System.Collections.Generic;
using UnityEngine;

public class Universe : MonoSingleton<Universe>
{
    [SerializeField] private float _gravitConstant = 0.001f;
    private List<Body> _bodies = new List<Body>();

    private void FixedUpdate()
    {
        for (int i = _bodies.Count - 1; i >= 0; i--)
        {
            Vector2 acceleration = CalculateAcceleration(_bodies[i]);
            _bodies[i].UpdateVelocity(acceleration);
        }
    }

    /// <summary>
    /// Calculate accelartion using 
    /// Newton's universal law of gravitation
    /// </summary>
    /// <param name="self"></param>
    /// <returns></returns>
    private Vector2 CalculateAcceleration(Body self)
    {
        var acceleration = Vector2.zero;
        for (int i = _bodies.Count - 1; i >= 0; i--)
        {
            if (_bodies[i] == self) continue;
            Vector2 difference = (_bodies[i].Position - self.Position);
            float sqrDst = difference.sqrMagnitude;
            Vector2 forceDir = difference.normalized;
            acceleration += forceDir * _gravitConstant * _bodies[i].Mass / sqrDst;
        }
        return acceleration;
    }


    public void AddBody(Body body)
    {
        if (_bodies.Contains(body)) return;
        _bodies.Add(body);
    }

    public void RemoveBody(Body body)
    {
        if (_bodies.Contains(body))
            _bodies.Remove(body);
    }

    private void Update()
    {
        for (int i = _bodies.Count - 1; i >= 0; i--)
        {
            _bodies[i].UpdatePosition();
        }
    }
}