﻿using UnityEngine;

namespace Assets.Scripts.GravityBalls
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField] private Body _bodyPrefab;
        [SerializeField] float _spawnInterval = 0.25f;
        [SerializeField] private TMPro.TMP_Text _spawnedBodiesCounter;
        private const int MAX_BODIES = 250;
        private int _currentBodiesCount = 0;
        private float _lastTimeSpawned;
        private Camera _camera;

        private void Awake()
        {
            _camera = Camera.main;
            if (_spawnedBodiesCounter == null)
                throw new System.NullReferenceException("there is no text reference");
        }

        private void Update()
        {
            if (AllowToSpawnBody())
            {
                SpawnBall();
            }
        }

        private bool AllowToSpawnBody()
        {
            return (Time.time > (_lastTimeSpawned + _spawnInterval)) && (_currentBodiesCount < MAX_BODIES);
        }

        private void SpawnBall()
        {
            var body = ObjectPoolerAutoResize<Body>.Instance.TakeFromPool(_bodyPrefab);
            var randomPosition = SpawnAtRandomPositionInCameraView();
            body.SetData(randomPosition, Vector3.zero);
            _currentBodiesCount++;

            _spawnedBodiesCounter.text = "Spawned bodies: " + _currentBodiesCount;
            _lastTimeSpawned = Time.time;
        }

        private Vector3 SpawnAtRandomPositionInCameraView()
        {
            return _camera.ScreenToWorldPoint(new Vector3(Random.Range(0, Screen.width), Random.Range(0, Screen.height), 10));
        }
    }
}
