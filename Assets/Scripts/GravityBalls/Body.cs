﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Body : MonoBehaviour
{
    [SerializeField] private float _radius;
    private Rigidbody2D _rigidbody;

    public float Radius
    {
        get => _radius;
        set {
            _radius = value;
            transform.localScale = Vector2.one * _radius;
        }
    }
    public float Mass => _radius * 10.0f;
    public Vector2 Velocity { get; private set; }
    public Vector2 Position => _rigidbody.position;


    private void Awake() // or init?
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        Universe.Instance?.AddBody(this);
    }

    private void OnDisable()
    {
        Universe.Instance?.RemoveBody(this);
    }

    public void SetData(Vector3 position, Vector3 initVelocity)
    {
        transform.position = position;
        _rigidbody.velocity = initVelocity;
    }

    public void UpdateVelocity(Vector2 acceleration)
    {
        Velocity += acceleration * Time.fixedDeltaTime;
    }

    public void UpdatePosition()
    {
        _rigidbody.MovePosition(_rigidbody.position + Velocity * Time.deltaTime); 
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var body = collision.gameObject.GetComponent<Body>();
        if (body == null) return;
        ManageBodiesCollistion(body);
    }
    private void ManageBodiesCollistion(Body body)
    {
        if (this.Mass > body.Mass)
        {
            ConsumeBody(body);
        }
        else
        {
            body.ConsumeBody(this);
        }
    }

    private void ConsumeBody(Body otherBody)
    {
        Radius += otherBody.Radius;
        ObjectPoolerAutoResize<Body>.Instance.ReleaseToPool(otherBody);
    }
}